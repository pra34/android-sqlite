package com.example.recorddkeeping;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {
	
	private ListView obj;
	DBHelper mydb;
	
	// byte[] imageInByte; // image byte is stored in this variable

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		mydb = new DBHelper(this);
		ArrayList<?> array_list = mydb.getAllInfo();
		
		// if the array_list size is greater than 0, then only we will display the arrayAdapter
		//if(array_list.size() > 0 ) {
		
			ArrayAdapter<?> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, array_list);
			
			//now we will add all the information fetch from the DB to the ListView of activity_main layout
			obj = (ListView)findViewById(R.id.listViewForRecords);
			obj.setAdapter(arrayAdapter);
			
			obj.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					int id_to_search = position + 1;
					Bundle dataBundle = new Bundle();
					dataBundle.putInt("id", id_to_search);
					
					Intent intent = new Intent(getApplicationContext(), com.example.recorddkeeping.DisplayRecord.class);
					intent.putExtras(dataBundle);
					
					startActivity(intent);
				}
			});
		//}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		
		super.onOptionsItemSelected(item);
		
		switch(item.getItemId()) {
			case R.id.item1:
				// code added for camera intent
				// if menu icon is clicked then camera intent should open to click the photo
				Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
				startActivityForResult(intent, 0); // here we are invoking camera intent onActivityForResult with request code 0
												   // here we are calling camera intent for result
				/*
				Intent displayRecordIntent = new Intent(getApplicationContext(), com.example.recorddkeeping.DisplayRecord.class);
				displayRecordIntent.putExtra("id", 0); // here we have added id 
														// if id = 0, then we are adding the record
														// if id > 0 then we are viewing the record
				displayRecordIntent.putExtra("imageByte", imageInByte); // here we have added byte image
				startActivity(displayRecordIntent);
				*/
				return true;
				
			default:
				return super.onOptionsItemSelected(item);
		}
		
	}
	
	// this method is invoked by startActivityForResult(), to get the camera byte image
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if(requestCode == 0){
			if(resultCode == RESULT_OK && data != null) {
		
				Bitmap bp; // image bitmap from camera is stored in this variable
				bp = (Bitmap) data.getExtras().get("data"); //here we have image bitmap from camera
			
				if(bp != null ) {
					
					Log.d("bp", bp.toString());
				
					// now we need to convert the bitmap image into byte image to send it to another intent
					//converting bitMap image into byte
					ByteArrayOutputStream stream = new ByteArrayOutputStream();
					bp.compress(Bitmap.CompressFormat.PNG, 100, stream);
					byte[] imageInByte = stream.toByteArray(); 
					
					Intent displayRecordIntent = new Intent(getApplicationContext(), DisplayRecord.class);
					displayRecordIntent.putExtra("id", 0); // here we have added id 
															// if id = 0, then we are adding the record
															// if id > 0 then we are viewing the record
					displayRecordIntent.putExtra("imageByte", imageInByte); // here we have added byte image
					startActivity(displayRecordIntent);
					
				}	
			}else if(resultCode == RESULT_CANCELED) {
				Toast.makeText(getApplicationContext(), "Picture taking Canceled",Toast.LENGTH_SHORT).show();
				Intent intent = new Intent(getApplicationContext(),com.example.recorddkeeping.MainActivity.class);
				startActivity(intent);
			}
		}
		else {
			// if image capture is failed
			Toast.makeText(getApplicationContext(), "Unexpected error occurred",Toast.LENGTH_SHORT).show();
			Intent backToMainActivity = new Intent(this, com.example.recorddkeeping.MainActivity.class);
			startActivity(backToMainActivity);
		}
	}
	
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_BACK) {
			moveTaskToBack(true);
		}
		return super.onKeyDown(keyCode, event);
	}
}
