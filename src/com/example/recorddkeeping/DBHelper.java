package com.example.recorddkeeping;

import java.sql.Blob;
import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBHelper extends SQLiteOpenHelper{
	
	public static final String DATABASE_NAME = "MyDBName.db";
	public static final String INFORMATION_TABLE_NAME = "information";
	public static final String INFORMATION_COLUMN_ID = "id";
	public static final String INFORMATION_COLUMN_IMAGE = "image";
	public static final String INFORMATION_COLUMN_NAME ="name";
	public static final String INFORMATION_COLUMN_FACULTY = "faculty";
	public static final String INFORMATION_COLUMN_PROGRAM = "program";
	public static final String INFORMATION_COLUMN_LEVEL = "level";
	//public static final String INFORMATION_COLUMN_PHONE = "phone";
	
	//private HashMap hp;
	
	public DBHelper(Context context) {
		super(context, DATABASE_NAME, null, 1);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		//db.execSQL("create table information " +
		//"(id integer primary key, image blob, name text, faculty text, program text, level text, phone text)");
		db.execSQL("create table information " +
		"(id integer primary key, image blob, name text, faculty text, program text, level text)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		db.execSQL("DROP TABLE IF EXISTS information");
		onCreate(db);
	}
	
	public boolean insertInfo(byte[] image, String name, String faculty, String program, String level) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues contentValues = new ContentValues();

		contentValues.put("image", image);
		contentValues.put("name", name);
		contentValues.put("faculty", faculty);
		contentValues.put("program", program);
		contentValues.put("level", level);
		//contentValues.put("phone", phone);
		
		long id = db.insert("information", null, contentValues);
		//db.close();
		
		return true;
	}
	
	public Cursor getData(int id) {
		SQLiteDatabase db = this.getReadableDatabase();
		//int database = db.OPEN_READONLY;
		//int database1 = db.OPEN_READWRITE;
		
		Cursor result = db.query(INFORMATION_TABLE_NAME, 
												new String[] { INFORMATION_COLUMN_ID,
												   INFORMATION_COLUMN_IMAGE, 
												   INFORMATION_COLUMN_NAME, 
												   INFORMATION_COLUMN_FACULTY,
												   INFORMATION_COLUMN_PROGRAM, 
												   INFORMATION_COLUMN_LEVEL},
									INFORMATION_COLUMN_ID + "=?",
									new String[] { String.valueOf(id) }, 
									null, null, null);			  
		if (result != null)
			   result.moveToFirst();
		
		//Cursor result2 = db.rawQuery("SELECT * FROM information WHERE id = 2", null);
		/*
		Cursor result = db.rawQuery("SELECT * FROM information WHERE id = "+id+"", null);
		
		Cursor result3 = db.rawQuery("SELECT * FROM information WHERE id = ?", new String[] { String.valueOf(id) });
		
		String query = "SELECT * FROM information WHERE id = " + id;
		
		Cursor result4 = db.rawQuery(query, null);
		
		int numberOfRecords = numberOfRows();
		
		ArrayList listOfRecords = getAllInfo();
		
		Cursor result5 = db.rawQuery("select * from information", null);
		
		Cursor result6 = getSingleRecord(id);
		
		
		Cursor result7 = db.query(INFORMATION_TABLE_NAME, 
								new String[] { INFORMATION_COLUMN_ID,
											   INFORMATION_COLUMN_IMAGE, 
											   INFORMATION_COLUMN_NAME, 
											   INFORMATION_COLUMN_FACULTY,
											   INFORMATION_COLUMN_PROGRAM, 
											   INFORMATION_COLUMN_LEVEL},
								INFORMATION_COLUMN_ID + "=?",
								new String[] { String.valueOf(id) }, 
								null, null, null);
		*/						
		//db.query(table, columns, selection, selectionArgs, groupBy, having, orderBy)
		result.moveToFirst();
		//db.close();
		return result;
	}
	
	public Cursor getSingleRecord(int id) {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor result = db.rawQuery("select * from information where id ="+id+"", null);
		return result;
	}
	
	public int numberOfRows() {
		SQLiteDatabase db = this.getReadableDatabase();
		int numRows = (int) DatabaseUtils.queryNumEntries(db, INFORMATION_TABLE_NAME);
		//db.close();
		return numRows;
	}
	
	public boolean updateInfo(Integer id, String name, String faculty, String program, String level) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		
		contentValues.put("name", name);
		contentValues.put("faculty", faculty);
		contentValues.put("program", program);
		contentValues.put("level", level);
		//contentValues.put("phone", phone);
		
		db.update("information", contentValues, "id = ? ", new String[] {Integer.toString(id)});
		//db.close();
		return true;
	}
	
	public boolean deleteInfo(Integer id) {
		SQLiteDatabase db = this.getReadableDatabase();
		
		db.delete("information", "id = ?", new String[] {Integer.toString(id)});
		//db.close();
		return true;
	}
	
	public ArrayList getAllInfo() {
		ArrayList array_list = new ArrayList();
		SQLiteDatabase db = this.getReadableDatabase();
		
		Cursor result = db.rawQuery("select * from information", null);
		
		result.moveToFirst();
		
		while(result.isAfterLast() == false) {
			array_list.add(result.getString(result.getColumnIndex(INFORMATION_COLUMN_NAME)));
			result.moveToNext();
		}
		Log.d("Array contain", array_list.toString());
		
		//db.close();
		return array_list;
	}

}
