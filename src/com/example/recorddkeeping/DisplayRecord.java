package com.example.recorddkeeping;

import java.io.ByteArrayInputStream;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

public class DisplayRecord extends ActionBarActivity{
	
	int invoked = 0;
	private DBHelper mydb = new DBHelper(this);
	ImageView imageView;
	TextView nameView, facultyView, programView, levelView;
	
	int id_to_update = 0;
	
	byte[] imageInByte; // image byte is stored in this variable
	Bitmap retrieveImageInBitmap; // retrieve image in bitMap
	
	// url for the server record inserting page
	private static String URL_INSERT_RECORD = "http://172.16.3.82/androidProject/insertRecord.php";
	
	// JSON parser class
    JSONParser jsonParser = new JSONParser();
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_display_record_new);
		
		Bundle extras = getIntent().getExtras();
		
		if(extras != null) {
			
			int getIdFromExtras = extras.getInt("id"); //getting id from invoking intent
			
			imageInByte = extras.getByteArray("imageByte"); //getting imageByte from invoking intent
			
			// finding all the view component of activity_display_record
			imageView = (ImageView) findViewById(R.id.imageView1);
			nameView = (TextView) findViewById(R.id.editTextName);
			facultyView = (TextView) findViewById(R.id.editTextFaculty);
			programView = (TextView) findViewById(R.id.editTextProgram);
			levelView = (TextView) findViewById(R.id.editTextLevel);
			//phoneView = (TextView) findViewById(R.id.editTextPhone);
			
				
			if(getIdFromExtras == 0) {
				
				// image byte array from main activity
				// we need to display this image in this content view
				// so convert byte image to bit image
				ByteArrayInputStream imageStreamFromMainActivity = new ByteArrayInputStream(imageInByte);		
				imageView.setImageBitmap(BitmapFactory.decodeStream(imageStreamFromMainActivity));
			
			}

			if(getIdFromExtras > 0) {
				
				//means this is the view part not the add record part.
				//mydb = new DBHelper(this);
				
				Cursor result = mydb.getData(getIdFromExtras); // fetching the required record
				Cursor result2 = mydb.getData(0);
				id_to_update = getIdFromExtras; // if we edit or delete the record we need the id of the record
				result.moveToFirst();
				
				//we have stored the image as byteImage so now we need to convert
				// the byteImage to bitMap image
				byte[] retrieveImageInByte = result.getBlob(result.getColumnIndex(DBHelper.INFORMATION_COLUMN_IMAGE));
				
				ByteArrayInputStream imageStream = new ByteArrayInputStream(retrieveImageInByte);
				retrieveImageInBitmap = BitmapFactory.decodeStream(imageStream);
				
				String name = result.getString(result.getColumnIndex(DBHelper.INFORMATION_COLUMN_NAME));
				String faculty = result.getString(result.getColumnIndex(DBHelper.INFORMATION_COLUMN_FACULTY));
				String program = result.getString(result.getColumnIndex(DBHelper.INFORMATION_COLUMN_PROGRAM));
				String level = result.getString(result.getColumnIndex(DBHelper.INFORMATION_COLUMN_LEVEL));
				//String phone = result.getString(result.getColumnIndex(DBHelper.INFORMATION_COLUMN_PHONE));
				
				if(!result.isClosed()) result.close(); // close cursor
				
				//this is a view part of the record, so we will make save button invisible
				Button saveButton = (Button) findViewById(R.id.save);
				saveButton.setVisibility(View.INVISIBLE);
				
				imageView.setImageBitmap(retrieveImageInBitmap); // we have set retriveImageInbitmap to imageView layout
				imageView.setFocusable(false);
				imageView.setClickable(false);
				
				nameView.setText((CharSequence)name);
				nameView.setFocusable(false);
				nameView.setClickable(false);
				
				facultyView.setText((CharSequence)faculty);
				facultyView.setFocusable(false);
				facultyView.setClickable(false);
				
				programView.setText((CharSequence)program);
				programView.setFocusable(false);
				programView.setClickable(false);
				
				levelView.setText((CharSequence)level);
				levelView.setFocusable(false);
				levelView.setClickable(false);
				
				/*
				phoneView.setText((CharSequence)phone);
				phoneView.setFocusable(false);
				phoneView.setClickable(false);
				*/
			}
		}
			
	} // end of onCreate method
	
	public boolean onCreateOptionsMenu(Menu menu) {
		//inflate the menu, here we will add edit and delete item in the action bar if it is present
		Bundle extras = getIntent().getExtras();
		if(extras != null){
			int getIdFromExtras = extras.getInt("id"); //getting id from invoking intent
			if(getIdFromExtras > 0) {
				// means this is a view record part, not the add part
				// so we will add edit and delete action in the menu
				getMenuInflater().inflate(R.menu.display_record, menu);
			} else {
				// means this is a add record part, we don't need to add edit and delete action in the action bar
				getMenuInflater().inflate(R.menu.main, menu);
			}
		}
		return true;
	}
	
	public boolean onOptionsItemSelected(MenuItem item) {
		super.onOptionsItemSelected(item);
		switch(item.getItemId())
		{
			case R.id.edit_record:
				
				Button saveButton = (Button)findViewById(R.id.save);
				saveButton.setVisibility(View.VISIBLE);
				
				nameView.setEnabled(true);
				nameView.setFocusableInTouchMode(true);
				nameView.setClickable(true);
				
				facultyView.setEnabled(true);
				facultyView.setFocusableInTouchMode(true);
				facultyView.setClickable(true);
				
				programView.setEnabled(true);
				programView.setFocusableInTouchMode(true);
				programView.setClickable(true);
				
				levelView.setEnabled(true);
				levelView.setFocusableInTouchMode(true);
				levelView.setClickable(true);
				
				/*
				phoneView.setEnabled(true);
				phoneView.setFocusableInTouchMode(true);;
				phoneView.setClickable(true);
				*/
			return true;

			case R.id.delete_record:
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setMessage(R.string.deleteInformation)
				.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						mydb.deleteInfo(id_to_update);
						Toast.makeText(getApplicationContext(), "Deleted Successfully",Toast.LENGTH_SHORT).show();
						Intent intent = new Intent(getApplicationContext(),com.example.recorddkeeping.MainActivity.class);
						finish();
						startActivity(intent);
						}
				})
				.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// User cancelled the dialog
					}
				});
	
				AlertDialog dialog = builder.create();
				dialog.setTitle("Are you sure ?");
				dialog.show();
			return true;
			
			//code added for sync record
			case R.id.sync_record:
				AlertDialog.Builder syncBuilder = new AlertDialog.Builder(this);
				syncBuilder.setMessage(R.string.syncInformation)
				.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						new SyncRecord().execute(); //invoking the SyncRecord class
					}
				})
				.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// User cancelled the dialog
					}
				});
				
				AlertDialog syncDialog = syncBuilder.create();
				syncDialog.setTitle("Are you sure ?");
				syncDialog.show();
			return true;
			
			default:
			return super.onOptionsItemSelected(item);
		}
	}// end of onOptionItemSelected method
	
	public void save(View view) {
		
		Bundle extras = getIntent().getExtras();
		if(extras != null) {
			int getIdFromExtras = extras.getInt("id"); //getting id from invoking intent
			if(getIdFromExtras > 0) {
				// means we are updating the record
				if(mydb.updateInfo(id_to_update,nameView.getText().toString(),
						facultyView.getText().toString(), programView.getText().toString(),
						levelView.getText().toString())){
					Toast.makeText(getApplicationContext(), "Updated", Toast.LENGTH_SHORT).show();
					/*
					// redirecting to main activity class
					Intent intent = new Intent(getApplicationContext(), com.example.recorddkeeping.MainActivity.class);
					finish();
					startActivity(intent);
					*/
				}else
					Toast.makeText(getApplicationContext(), "not Updated", Toast.LENGTH_SHORT).show();
			} else {
				// means we are inserting the record
				if(mydb.insertInfo(imageInByte, nameView.getText().toString(),
						facultyView.getText().toString(), programView.getText().toString(),
						levelView.getText().toString())){
					Toast.makeText(getApplicationContext(), "Inserted", Toast.LENGTH_SHORT).show();
					/*
					// redirecting to main activity class
					Intent intent = new Intent(getApplicationContext(), com.example.recorddkeeping.MainActivity.class);
					finish();
					startActivity(intent);
					*/
				}else
					Toast.makeText(getApplicationContext(), "not Inserted", Toast.LENGTH_SHORT).show();
			}
			// redirecting to main activity class after displaying error toast
			Intent intent = new Intent(getApplicationContext(), com.example.recorddkeeping.MainActivity.class);
			finish();
			startActivity(intent);
		}
	}

	//inner class SyncRecord
	class SyncRecord extends AsyncTask<String, String, String>{
		
		ProgressDialog pDialog;
		
		// before starting background thread show progress dialog
		protected void onPreExecute() {
			super.onPreExecute();
			
			pDialog = new ProgressDialog(DisplayRecord.this);
			pDialog.setMessage("Saving record ....");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... arg0) {
			// TODO Auto-generated method stub
			//building parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();

			params.add(new BasicNameValuePair("image", imageInByte.toString()));
			params.add(new BasicNameValuePair("name", nameView.getText().toString()));
			params.add(new BasicNameValuePair("faculty", facultyView.getText().toString()));
			params.add(new BasicNameValuePair("program", programView.getText().toString()));
			params.add(new BasicNameValuePair("level", levelView.getText().toString()));

			// getting JSON Object
			// Note that insert record url accepts POST method
			JSONObject json = jsonParser.makeHttpRequest(URL_INSERT_RECORD, "POST", params);

			//check log for response
			Log.d("Create Response ", json.toString());

			//check for the success tag 
			try {
				
				//reading data from returned json object
				int successCode = json.getInt("success");
				String message = json.getString("message");

					if(successCode == 1) {
						// successfully inserted record
						Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

						Intent intent = new Intent(getApplicationContext(),com.example.recorddkeeping.MainActivity.class);
						finish();
						startActivity(intent);
						// closing this screen
						finish();
					} else {
						// failed to insert the record
						Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
						Log.d("failed to insert the record", json.toString());

						Intent intent = new Intent(getApplicationContext(),com.example.recorddkeeping.MainActivity.class);
						finish();
						startActivity(intent);
					}
				} catch(JSONException e) {
					e.printStackTrace();
				}
			return null;
		}
		
		// before starting background thread show progress dialog
			protected void onPostExecute(String file_url) {
				pDialog.dismiss();
			}

	}

}
